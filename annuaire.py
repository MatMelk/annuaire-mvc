#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 15:22:29 2020

@author: m16035433
"""

import tkinter as tk
import pickle as pkl
from os import listdir


class Model:
    
    def __init__(self):
        if 'annuaire.p' in listdir():
            self.d=pkl.load(open( "annuaire.p",'rb'))
        else:
            self.d={}
        
    def Chercher(self,nom):
        prenom=self.d[nom][0]
        tel=self.d[nom][1]
        ad=self.d[nom][2]
        ville=self.d[nom][3]
        return prenom,tel,ad,ville
        
    def Inserer(self,nom,prenom,tel,ad,ville):
        self.d[nom]=(prenom,tel,ad,ville)
        pkl.dump(self.d,open("annuaire.p","wb" ))
        
        
class Vue:
    
    def __init__(self,master):
        #master
        self.master=master
        #controleur
        self.c=Controleur(self)
        #frame
        self.info=tk.Frame()
        self.info.grid(row=0,column=0)
        self.entry=tk.Frame()
        self.entry.grid(row=0,column=1)
        self.act=tk.Frame()
        self.act.grid(row=1,column=0,columnspan=2)
        #stringvar
        self.nom=tk.StringVar()
        self.prenom=tk.StringVar()
        self.tel=tk.StringVar()
        self.ad=tk.StringVar()
        self.ville=tk.StringVar()
        #label
        self.nom1=tk.Label(self.info,text="Nom").grid(sticky='e')
        self.prenom1=tk.Label(self.info,text="Prénom").grid(sticky='e')
        self.tel1=tk.Label(self.info,text="Téléphone").grid(sticky='e')
        self.ad1=tk.Label(self.info,text="Adresse").grid(sticky='e')
        self.ville1=tk.Label(self.info,text="Ville").grid(sticky='e')
        #entry
        self.nom2=tk.Entry(self.entry,textvar=self.nom).pack()
        self.prenom2=tk.Entry(self.entry,textvar=self.prenom).pack()
        self.tel2=tk.Entry(self.entry,textvar=self.tel).pack()
        self.ad2=tk.Entry(self.entry,textvar=self.ad).pack()
        self.ville2=tk.Entry(self.entry,textvar=self.ville).pack()
        #button
        self.chercher=tk.Button(self.act,text="Chercher",command=self.c.Chercher).pack(side="left")
        self.inserer=tk.Button(self.act,text="Inserer",command=self.c.Inserer).pack(side="left")
        self.effacer=tk.Button(self.act,text="Effacer",command=self.Effacer).pack(side="left")
        
    def Effacer(self):
        self.nom.set('')
        self.prenom.set('')
        self.tel.set('')
        self.ad.set('')
        self.ville.set('')


class Controleur:
    
    def __init__(self,vue):
        self.vue=vue
        self.model=Model()
        
    def Chercher(self):
        if self.vue.nom.get() in self.model.d:
            cherch=self.model.Chercher(self.vue.nom.get())
            self.vue.prenom.set(cherch[0])
            self.vue.tel.set(cherch[1])
            self.vue.ad.set(cherch[2])
            self.vue.ville.set(cherch[3])
            
    def Inserer(self):
        self.model.Inserer(self.vue.nom.get(),self.vue.prenom.get(),self.vue.tel.get(),self.vue.ad.get(),self.vue.ville.get())


class Commande:
    
    def __init__(self):
        self.master=tk.Tk()
        self.nom=tk.StringVar()
        self.prenom=tk.StringVar()
        self.tel=tk.StringVar()
        self.ad=tk.StringVar()
        self.ville=tk.StringVar()        
        self.c=Controleur(self)
        
    def Chercher(self,nom):
        self.nom.set(nom)
        self.c.Chercher()
        return self.nom.get() + '\n' + self.prenom.get() + '\n' + self.tel.get() + '\n' + self.ad.get() + '\n' + self.ville.get() + '\n'
    
    def Inserer(self,nom,prenom,tel,ad,ville):
        self.nom.set(nom)
        self.prenom.set(prenom)
        self.tel.set(tel)
        self.ad.set(ad)
        self.ville.set(ville)
        self.c.Inserer()


if __name__ == "__main__" :
    com=Commande()
    com.Inserer('Lagaffe','gaston','07 45 67 38 29','25 rue de la farce','Tour')
    print(com.Chercher('Lagaffe'))
    annuaire=tk.Tk()
    v=Vue(annuaire)
    annuaire.mainloop()
    






















        